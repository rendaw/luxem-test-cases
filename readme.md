# Luxem test cases

A compliant parser should be able to parse all the files in `valid/`, and should raise an error when parsing any file in `invalid/`.
